DESCRIPTION
=================
Bulk Delete Nodes allows bulk deletion of nodes by specified filters either 
via Drupal Batch or Queue API.

INSTALLATION
=================
Copy bulk_delete_nodes into your sites modules direcotry. Go to Administer > 
Modules and enable Bulk Delete Nodes.

CONFIGURATION
=================
Go to Administer > Content > Bulk Delete Nodes > Settings. Change Batch loop 
size and/or Queue execution time if needed.

USAGE
=================
Go to Administer > Content > Bulk Delete Nodes. Specify the filters and delete 
mode and click on Delete Nodes button. Confirmation page will warn you how 
many nodes will be deleted. You can then proceed with Delete button or return 
with Cancel.
