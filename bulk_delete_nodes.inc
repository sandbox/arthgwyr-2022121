<?php
/**
 * @file
 * Administration functions for bulk_delete_nodes module.
 */

/**
 * Returns form for path admin/content/bulk_delete_nodes.
 */
function bulk_delete_nodes_form($form, &$form_state) {
  if ($form_state['rebuild']) {
    return bulk_delete_nodes_confirm_form($form_state);
  }

  $form['#attached'] = array(
    'css' => array(drupal_get_path('module', 'bulk_delete_nodes') . '/bulk_delete_nodes.css'),
  );

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node delete filters'),
    '#collapsible' => FALSE,
    '#weight' => 1,
  );
  $form['filters']['type'] = array(
    '#type' => 'select',
    '#title' => t('type'),
    '#options' => array(),
    '#default_value' => 'any',
    '#prefix' => '<div class="bulk-delete-nodes-filter-cont">',
    '#suffix' => '</div>',
    '#weight' => 1,
  );
  $form['filters']['status'] = array(
    '#type' => 'select',
    '#title' => t('status'),
    '#options' => array(),
    '#default_value' => 'any',
    '#prefix' => '<div class="bulk-delete-nodes-filter-cont">',
    '#suffix' => '</div>',
    '#weight' => 2,
  );
  $form['filters']['user'] = array(
    '#type' => 'select',
    '#title' => t('user'),
    '#options' => array(),
    '#default_value' => 'any',
    '#prefix' => '<div class="bulk-delete-nodes-filter-cont">',
    '#suffix' => '</div>',
    '#weight' => 3,
  );

  // Creation date.
  $form['filters']['creation'] = array(
    '#type' => 'fieldset',
    '#title' => t('creation date'),
    '#weight' => 5,
  );
  $form['filters']['creation']['enable_creation_date'] = array(
    '#type' => 'checkbox',
    '#title' => t('enable'),
  );
  $form['filters']['creation']['container'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="enable_creation_date"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['filters']['creation']['container']['start_date'] = array(
    '#type' => 'date',
    '#title' => t('start date'),
    '#prefix' => '<div class="bulk-delete-nodes-creation-date">',
    '#suffix' => '</div>',
    '#weight' => 1,
  );
  $form['filters']['creation']['container']['end_date'] = array(
    '#type' => 'date',
    '#title' => t('end date'),
    '#prefix' => '<div class="bulk-delete-nodes-creation-date">',
    '#suffix' => '</div> <div class="bulk-delete-nodes-bclear"></div>',
    '#weight' => 2,
  );

  // Delete mode.
  $form['mode'] = array(
    '#type' => 'fieldset',
    '#title' => t('Delete Mode'),
    '#collapsible' => FALSE,
    '#weight' => 2,
  );
  $form['mode']['delete_mode'] = array(
    '#type' => 'radios',
    '#description' => t('You can create queue for deleting nodes later using Drupal Queue API.'),
    '#options' => array(
      'batch' => t('Delete nodes via Batch API'),
      'queue' => t('Delete nodes via Queue API'),
    ),
    '#default_value' => 'batch',
  );

  if ($languages = module_invoke('locale', 'language_list')) {
    $languages = array(LANGUAGE_NONE => t('Language neutral')) + $languages;

    $options = array('any' => t('any')) + $languages;

    $form['filters']['language'] = array(
      '#type' => 'select',
      '#title' => t('language'),
      '#options' => $options,
      '#default_value' => t('any'),
      '#prefix' => '<div class="bulk-delete-nodes-filter-cont">',
      '#suffix' => '</div>',
      '#weight' => 4,
    );
  }

  // Initialize node types.
  $types = node_type_get_names();
  $options = array('any' => t('any')) + node_type_get_names();
  $form['filters']['type']['#options'] = $options;

  // Initialize node statuses.
  $options = array(
    'any' => t('any'),
    'published' => t('published'),
    'not-published' => t('not published'),
    'promoted' => t('promoted'),
    'not-promoted' => t('not promoted'),
    'sticky' => t('sticky'),
    'not-sticky' => t('not sticky'),
  );
  if (module_exists('translation')) {
    $options += array(
      'translate-0' => t('Up to date translation'),
      'translate-1' => t('Outdated translation'),
    );
  }
  $form['filters']['status']['#options'] = $options;

  // Initialize users.
  $users = bulk_delete_nodes_get_nodes_users();
  $options = array('any' => t('any')) + $users;
  $form['filters']['user']['#options'] = $options;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete Nodes'),
    '#weight' => 3,
  );

  return $form;
}

/**
 * Validate function for bulk_delete_nodes_form. 
 */
function bulk_delete_nodes_form_validate($form, &$form_state) {
  // We needn't validate confirmation form.
  if ($form_state['clicked_button']['#id'] == 'edit-submit-confirm') {
    return;
  }

  $values = $form_state['values'];
  $has_error = FALSE;

  if ($values['type'] == 'any' && $values['status'] == 'any' && $values['user'] == 'any' &&
((!isset($values['language'])) || (isset($values['language']) && $values['language'] == 'any')) &&
$values['enable_creation_date'] == 0) {
    form_set_error('type', t('You should choose some filters'));
    $has_error = TRUE;
  }
  else {
    // If creation dates are enabled.
    if ($values['enable_creation_date'] == 1) {
      $start_date = $values['start_date'];
      $end_date = $values['end_date'];

      $start_time = mktime(0, 0, 0, $start_date['month'], $start_date['day'], $start_date['year']);
      $end_time = mktime(0, 0, 0, $end_date['month'], $end_date['day'], $end_date['year']);
      $now = mktime(0, 0, 0, date('m'), date('d'), date('Y'));

      if ($end_time > $now) {
        form_set_error('end_date', t('Not a valid creation end date'));
        $has_error = TRUE;
      }
      // Check if dates are crossing.
      elseif ($start_time > $end_time) {
        form_set_error('start_date', t('Not a valid creation start date'));
        form_set_error('end_date', t('Not a valid creation end date'));
        $has_error = TRUE;
      }
    }
  }

  if (!$has_error) {
    $form_state['rebuild'] = TRUE;

    if ($values['enable_creation_date'] == 1) {
      $form_state['rebuild_info'] = array(
        'start_time' => $start_time,
        'end_time' => $end_time,
      );
    }
  }
}

/**
 * Submit function for bulk_delete_nodes_form after confirmation.
 */
function bulk_delete_nodes_form_submit($form, &$form_state) {
  $where_condition = $form_state['query_where']['condition'];
  $nids = $form_state['query_where']['nids'];
  $nodes_number = count($nids);

  $values = $form_state['values'];
  $delete_mode = $values['delete_mode'];

  if ($delete_mode == 'batch') {
    // Delete nodes with batch.
    $batch = array(
      'title' => t('Deleting Nodes'),
      'init_message' => t('Deleting nodes starts'),
      'progress_message' => t('Deleting nodes...'),
      'error_message' => t('Error of deleting nodes'),
      'operations' => array(
        array('bulk_delete_nodes_operation_delete', array($nids)),
      ),
      'finished' => 'bulk_delete_nodes_operation_delete_finish',
      'file' => drupal_get_path('module', 'bulk_delete_nodes') . '/bulk_delete_nodes.inc',
    );

    batch_set($batch);
  }
  elseif ($delete_mode == 'queue') {
    // Delete nodes with queue.
    $result = db_update('node')
      ->fields(array('status' => 0))
      ->where($where_condition)
      ->execute();
    $unpublished_nodes_number = $result->rowCount();
    if ($unpublished_nodes_number != $nodes_number) {
      drupal_set_message(t('Error unpublishing nodes.'), 'error');
    }
    else {
      $queue = DrupalQueue::get('bulk_delete_nodes_queue');

      foreach ($nids as $nid) {
        $queue->createItem($nid);
      }

      drupal_set_message(t('@nodes nodes are added to the queue and will be deleded when the cron job run.', array(
        '@nodes' => $nodes_number,
      )));
    }
  }
}

/**
 * Batch delete nodes callback function.
 */
function bulk_delete_nodes_operation_delete($nids, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node_index'] = 0;
    $context['sandbox']['max'] = count($nids);
  }

  $limit = variable_get('bulk_delete_nodes_batch_size', BULK_DELETE_NODES_DEFAULT_BATCH_SIZE);

  $operate_nids = array_slice($nids, $context['sandbox']['current_node_index'], $limit);
  node_delete_multiple($operate_nids);

  $context['sandbox']['progress'] += $limit;
  $context['sandbox']['current_node_index'] += $limit;
  if ($context['sandbox']['progress'] > $context['sandbox']['max']) {
    $context['sandbox']['progress'] = $context['sandbox']['max'];
  }

  $context['message'] = t('Deleted %current of %nodes', array(
    '%current' => $context['sandbox']['progress'],
    '%nodes'   => $context['sandbox']['max'],
  ));
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
  else {
    $context['results']['max'] = $context['sandbox']['max'];
  }
}

/**
 * Batch delete nodes finish function.
 */
function bulk_delete_nodes_operation_delete_finish($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('@nodes nodes deleted successfully.', array('@nodes' => $results['max'])));
  }
  else {
    drupal_set_error(t('Finished with error.'));
  }
}

/**
 * Returns delete nodes confirmation form.
 */
function bulk_delete_nodes_confirm_form(&$form_state) {
  $values = $form_state['values'];
  $nodes_number = NULL;
  $where = array();

  if ($values['type'] != 'any') {
    $where[] = 'n.type = \'' . $values['type'] . '\'';
  }
  if ($values['status'] != 'any') {
    switch ($values['status']) {
      case 'published':
        $where[] = 'n.status = 1';
        break;

      case 'not-published':
        $where[] = 'n.status = 0';
        break;

      case 'promoted':
        $where[] = 'n.promote = 1';
        break;

      case 'not-promoted':
        $where[] = 'n.promote = 0';
        break;

      case 'sticky':
        $where[] = 'n.sticky = 1';
        break;

      case 'not-sticky':
        $where[] = 'n.sticky = 0';
        break;
    }
  }
  if ($values['user'] != 'any') {
    $where[] = 'n.uid = \'' . $values['user'] . '\'';
  }
  if ($languages = module_invoke('locale', 'language_list')) {
    if ($values['language'] != 'any') {
      $where[] = 'n.language = \'' . $values['language'] . '\'';
    }
  }
  if ($values['enable_creation_date'] == 1) {
    $start_time = $form_state['rebuild_info']['start_time'];
    $end_time = $form_state['rebuild_info']['end_time'];

    $where[] = '(
				 created >= ' . $start_time . ' AND
			     created <= ' . $end_time . '
			    )';
  }

  $query_where = '';
  $where_num = count($where) - 1;
  foreach ($where as $index => $condition) {
    $query_where .= $condition;
    if ($index != $where_num) {
      $query_where .= ' AND ';
    }
  }

  $query = db_select('node', 'n');
  $query->addField('n', 'nid');
  $query->where($query_where);
  $result = $query->execute();
  $nodes_number = $result->rowCount();
  if ($nodes_number != 0) {
    $nids = $result->fetchAllAssoc('nid');
    $nids = array_keys($nids);
    $form_state['query_where'] = array(
      'condition' => $query_where,
      'nids' => $nids,
    );
  }

  // Configuring form.
  $form = array();

  $text = t('This will affect <b><big>@nodes_number</big></b> nodes.<br />', array('@nodes_number' => $nodes_number));
  if ($nodes_number != 0) {
    $text .= t('Please confirm to process.');
  }

  if ($nodes_number != 0) {
    $form['delete_mode'] = array(
      '#type' => 'hidden',
      '#value' => $values['delete_mode'],
      '#required' => TRUE,
    );
  }

  $form['nodes_number'] = array(
    '#markup' => '<p>' . $text . '</p>',
    '#weight' => 1,
  );
  $form['submit_confirm'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#suffix' => l(t('Cancel'), 'admin/content/bulk_delete_nodes'),
    '#weight' => 2,
  );
  if ($nodes_number == 0) {
    $form['submit_confirm']['#disabled'] = TRUE;
  }

  return $form;
}

/**
 * Get users which have nodes.
 */
function bulk_delete_nodes_get_nodes_users() {
  $query = db_select('node', 'n');
  $query->addJoin('LEFT', 'users', 'u', 'n.uid = u.uid');
  $query->addField('n', 'uid', 'user_id');
  $query->addField('u', 'name', 'login');
  $query->distinct();
  $result = $query->execute();
  $users = array();
  while ($array = $result->fetchAssoc()) {
    $users[$array['user_id']] = $array['login'];
  }

  return $users;
}

/**
 * Returns form for path admin/content/bulk_delete_nodes/config.
 */
function bulk_delete_nodes_config_form($form, &$form_state) {
  $form = array();

  $form['#attached'] = array(
    'css' => array(drupal_get_path('module', 'bulk_delete_nodes') . '/bulk_delete_nodes.css'),
  );

  $form['back_link'] = array(
    '#markup' => '<p>' . l(t('Delete Nodes'), 'admin/content/bulk_delete_nodes') . '</p>',
  );

  $form['batch_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Batch loop size'),
    '#default_value' => variable_get('bulk_delete_nodes_batch_size', BULK_DELETE_NODES_DEFAULT_BATCH_SIZE),
    '#size' => 3,
    '#weight' => 1,
  );

  $form['queue_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Queue execution limit in seconds'),
    '#default_value' => variable_get('bulk_delete_nodes_queue_time', BULK_DELETE_NODES_DEFAULT_QUEUE_TIME),
    '#size' => 3,
    '#weight' => 2,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 3,
  );

  return $form;
}

/**
 * Validate function for bulk_delete_nodes_config_form.
 */
function bulk_delete_nodes_config_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  if (!ctype_digit($values['batch_size'])) {
    form_set_error('batch_size', t('Not a valid batch limit'));
  }

  if (!ctype_digit($values['queue_time'])) {
    form_set_error('queue_time', t('Not a valid queue time'));
  }
}

/**
 * Submit function for bulk_delete_nodes_config_form.
 */
function bulk_delete_nodes_config_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $batch_limit = $values['batch_size'];
  $queue_time = $values['queue_time'];

  variable_set('bulk_delete_nodes_batch_size', $batch_limit);
  variable_set('bulk_delete_nodes_queue_time', $queue_time);

  drupal_set_message(t('Your changes have been saved'));
}
